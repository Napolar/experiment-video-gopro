<div align="center">
  <a href="https://fr.gopro.com/" target="_blank" rel="noopener noreferrer">
  <img src="https://i2.wp.com/salondelaphoto.ca/wp-content/uploads/2015/09/logo-GoPro.png" width="140" alt="Directus Logo"/>
  </a>
</div>

<p>&nbsp;</p>

<div align="center">
  <h1>
    GoPro Experiments
  </h1>
</div>

<div align="center">
  <h3>
    <a href="http://thomasfel.fr">Website</a> •  
    <a href="http://thomasfeaw.cluster021.hosting.ovh.net/gopro/">Demo</a> • 
    <a href="https://twitter.com/napoolar">Twitter</a>
  </h3>
</div>

<p>&nbsp;</p>

> _The idea of this draft was to offer to the users entry points other than time on his video. In this example, I choosed speed and localisation._

## Quick Start

```sh
npm install
npm run start
```

Then open [http://localhost:8080/](http://localhost:8080/) to see the demo.<br>
If you want to see the production and the minified bundle, run `npm run build`.

## Details

Concerning the speed, a graph is present at the bottom of the video and corresponds to the real-time speed of the user, he can then go to a point in the video by clicking on the graph.

Concerning the position, a map is present at the top right of the video and corresponds to the real-time position of the user, by clicking on a point on his route, he can go to the moment of the video where he was closest to that point.

## To production...

this is a draft in the strict and limited sense of the word.
But, in case, this is a non-exhaustive list of points that should be improved:

-   browser compatibility
-   cost of Mapbox API (free when under 50,000 views)
-   case when the road pass multiples times on the same point
-   case of zero speed (one could detect a d(v)/t and hide the graph ?)
-   ...

## Specifications

-   operating system: Ubuntu 18.04 Bionic
-   node.js version: v11.5.0
-   browser: Chrome 71.0.3578.98 (Official Build)

## Acknowledgements

Some data were extracted using **GPMF-Parser**.
I am grateful to this related projects for their ideas and collaboration:

-   [@gopro](https://github.com/gopro/gpmf-parser)
-   [@chartjs](https://github.com/chartjs)
-   [@google](https://github.com/google/material-design-icons)
-   [@sampotts](https://github.com/sampotts/plyr)
-   [@mapbox](https://github.com/mapbox)

🖐 Thomas FEL, 2019
