import "./styles/index.scss";
import "./assets/videos/video.webm";
import "./assets/me.jpeg";
import "./assets/location.png";
import Graph from "./classes/Graph";
import Point from "./classes/Point";
import jsonData from "./assets/data/gps.json";
import Player from "./classes/Player";
import CustomMap from "./classes/Map";
import gpsData from "./assets/data/coordonates.json";

// Import data.
const data = jsonData.speed.samples;
// Create array of points.
const points = data.map(arr => new Point(arr[0], arr[1]));
// Create callback for binding Video to the Graph.
const onProgress = newPourcentage => {
	graph.updateMaskChartPourcentage(newPourcentage);
	map.updatePoint(newPourcentage);
};
const onClickOnGraph = newPourcentage => {
	player.setTimeInPourcentage(newPourcentage);
	map.updatePoint(newPourcentage);
};
const onClickOnMap = newPourcentage => {
	graph.updateMaskChartPourcentage(newPourcentage);
	player.setTimeInPourcentage(newPourcentage);
};
// Create graph, a video player and a map.
const graph = new Graph(points, "speedCanvas", onClickOnGraph, "speedSpan");
const player = new Player("player", onProgress);
const map = new CustomMap("map-container", gpsData, onClickOnMap);

// Event on click to show/hide the graph.
document.querySelector("#toggleGraph").addEventListener("click", e => {
	document.querySelector(".graph-container").classList.toggle("actif");
});
// Event on click to show/hide the map.
document.querySelector("#toggleMap").addEventListener("click", e => {
	document.querySelector("#map-container").classList.toggle("d-none");
});

// Start the app.

setTimeout(() => {
	// Page chargée...
	const loader = document.querySelector(".loader-container");
	const main = document.querySelector(".main");
	loader.className += " hide";
	setTimeout(() => {
		loader.className += " d-none";
		main.className = main.className.replace("hide", "");
	}, 300);
}, 2500);
