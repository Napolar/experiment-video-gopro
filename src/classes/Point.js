class Point {
	/**
	 * Construct and return a simple Point
	 * @param {float} x
	 * @param {float || null} y
	 * ! Y param can be null.
	 */
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}

	distance(point) {
		let deltaX = Math.abs(point.x - this.x);
		let deltaY = Math.abs(point.y - this.y);
		return Number(deltaX) + Number(deltaY);
	}

	toString() {
		return this.x + "," + this.y;
	}
	toArray() {
		return [this.x, this.y].map(Number);
	}
	toGeoJson() {
		return {
			type: "Point",
			coordinates: [this.x, this.y]
		};
	}
	static stringToPoint(s) {
		try {
			return new Point(s.split(",")[0], s.split(",")[1]);
		} catch (e) {
			throw e;
		}
	}
}

export default Point;
