import Mapboxgl from "mapbox-gl";
import marker from "../assets/location.png";
import Point from "./Point";
import { bezierSpline, lineString } from "@turf/turf";
import { lineaire } from "../utils/interpolations";
import { getLineLayer, getMarkerLayer } from "../utils/mapOptions";

const accessToken = "pk.eyJ1IjoibmFwb2xhciIsImEiOiJjanNmeTBwbWExajduNGFsbmlpdDQzN3I2In0.DtH7Pi5gPgL5D-bbPF67LQ";
const defaultStyle = "mapbox://styles/mapbox/light-v10";
const defaultSize = "100px"; // Height and Width of the mini-map.
const maxSize = "300px"; // Height and Width of the map in max size mode.
const iconId = "marker";
const sourceId = "position";

/**
 * Custom Map based on MapBox-GL
 * @param id : container of the map. (DOM node ID)
 * @param coordonates[] : list of object, every object must contains deux keys : 'lat' and 'lng'.
 */
class CustomMap {
	constructor(id, coordonates, callbackOnClick) {
		this.domNode = document.querySelector("#" + id);
		window.fullscreen = () => {
			this.toggleSize();
		};
		Mapboxgl.accessToken = accessToken;
		this.coordonates = coordonates;
		this.callbackOnClick = callbackOnClick;
		this.points = this.coordonates.map(c => new Point(c.lng, c.lat));
		this.map = new Mapboxgl.Map({
			container: id,
			style: defaultStyle
		});
		this.interpolatePoints = this.getInterpolatePoints();
		this.map.doubleClickZoom.disable();
		this.toggleSize();
		this.map.on("load", () => {
			this.didMount();
		});
		this.map.on("dblclick", "path", e => this.click(e));
	}

	toggleSize() {
		if (typeof this.size !== "string") this.size = defaultSize;
		// On applque la nouvelle taille au container.
		this.domNode.style.width = this.size;
		this.domNode.style.height = this.size;
		// On change la taille pour la prochaine fois
		this.size = this.size === defaultSize ? maxSize : defaultSize;
		setTimeout(() => {
			// On laisse le temps a la map d'effectuer sa transition (100ms)
			this.map.resize();
			// Centrage de notre map autour de la route.
			this.map.fitBounds([this.interpolatePoints[0].toArray(), this.interpolatePoints[this.interpolatePoints.length - 1].toArray()], {
				padding: 10
			});
		}, 110);
	}

	correctMapSettings() {
		// On enlève les bouttons de bases qui gênent.
		document.querySelector("#map-container > div.mapboxgl-control-container > div.mapboxgl-ctrl-bottom-right").className += " d-none";
		document.querySelector("#map-container > div.mapboxgl-control-container > div.mapboxgl-ctrl-bottom-left").className += " d-none";
		// Ajout du button fullscreen
		document.querySelector("#map-container > div.mapboxgl-control-container > div.mapboxgl-ctrl-top-left").innerHTML = `
			<div class="button" onclick='fullscreen()'>
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M6 14c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1H7v-2c0-.55-.45-1-1-1zm0-4c.55 0 1-.45 1-1V7h2c.55 0 1-.45 1-1s-.45-1-1-1H6c-.55 0-1 .45-1 1v3c0 .55.45 1 1 1zm11 7h-2c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1v-3c0-.55-.45-1-1-1s-1 .45-1 1v2zM14 6c0 .55.45 1 1 1h2v2c0 .55.45 1 1 1s1-.45 1-1V6c0-.55-.45-1-1-1h-3c-.55 0-1 .45-1 1z"/></svg>
			</div>
		`;
	}

	getInterpolatePoints() {
		// On copie les coordonates
		let coordonates = [...this.coordonates];
		let startIndex, endIndex, startValue, endValue;
		// Nous allons interpoler pour latitude et longitude O(n)
		["lat", "lng"].map(dimension => {
			startIndex = 0;
			endIndex = 0;
			while (endIndex != "fin") {
				// tant que la liste n'est pas vérifier jusqu'au bout.
				startValue = coordonates[startIndex][dimension];
				for (let i = startIndex; i < coordonates.length; i++) {
					// On parcours la liste jusqu'a trouver un changement de notre dimension.
					if (coordonates[i][dimension] != startValue) {
						// On enregistre l'index ou celle-ci a changé.
						endIndex = i;
						endValue = coordonates[i][dimension];
						break;
					}
					// Aucun changement, donc fin de liste, on a terminer notre travail
					endIndex = "fin";
				}
				//* On effectue pas de changement si on a atteint la fin de liste, car on a besoin d'au moins 2 point différents pour interpoler.
				//* amélioration possible...
				if (endIndex === "fin") break;
				// On interpole de manière linéaire entre deux points, et leur index.
				let indexDelta = endIndex - startIndex;
				let valueDelta = endValue - startValue;
				for (let i = 0; i < indexDelta; i++) {
					// simple interpolation linéaire
					coordonates[i + startIndex][dimension] += (valueDelta / indexDelta) * i;
				}
				// On passe a la valeur suivante
				startIndex = endIndex;
			}
		});
		return coordonates.map(c => new Point(c.lng, c.lat));
	}

	getBezierLines() {
		// On construit une ligne avec l'api de Mapbox (turf)
		let line = lineString(this.uniquesPoints.map(p => p.toArray()));
		// On la courbe
		let curve = bezierSpline(line, { resolution: 1000, sharpness: 0.1 });
		// On renvoie simplement les données brutes de la courbe
		return curve.geometry.coordinates;
	}

	didMount() {
		// La map est à l'écran.
		// Construction de la ligne (route)
		this.map.addLayer(getLineLayer(this.interpolatePoints, "#414151"));
		// Ajout de la source de données de la position
		this.map.addSource(sourceId, {
			type: "geojson",
			data: this.interpolatePoints[0].toGeoJson()
		});
		// On charge l'image qui servira pour le marker.
		this.map.loadImage("./location.png", (err, img) => {
			if (err) throw err;
			this.map.addImage(iconId, img);
			// Ajout du marker
			this.map.addLayer(getMarkerLayer(sourceId, iconId));
		});
		// Correction de notre map. (re-centrage, action sur les boutons)
		this.correctMapSettings();
	}
	click(e) {
		const desiredCoordonates = new Point(e.lngLat.lng, e.lngLat.lat);
		// We try to find a point who could be beetwen two other.
		let bestIndex = 0;
		this.interpolatePoints.reduce(function(mem, curr, index) {
			if (mem.distance(desiredCoordonates) < curr.distance(desiredCoordonates)) {
				// Le point en mémoire est plus proche du point testé.
				return mem;
			} else {
				// Le poin testé (courant) est plus proche du point desiré, on le met en mémoire
				bestIndex = index;
				return curr;
			}
		});
		// A partir de l'index, on de la liste, qui est lui proportionelle au temps, on en deduit le pourcentage de la vidéo.
		let newPourcentage = bestIndex / this.interpolatePoints.length;
		this.updatePoint(newPourcentage);
		this.callbackOnClick(newPourcentage);
	}

	updatePoint(pourcentage) {
		// Le point vient de change de position.
		let newPoint = this.interpolatePoints[Math.floor(pourcentage * this.interpolatePoints.length)];
		this.map.getSource(sourceId).setData(newPoint.toGeoJson());
	}
}

export default CustomMap;
