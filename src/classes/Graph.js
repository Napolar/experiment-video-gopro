import Point from "./Point";
import { lineaire } from "../utils/interpolations";
import Chart from "chart.js";
import chartOptions from "../utils/chartOptions";

const augmentation = 10;

class Graph {
	/**
	 * Construct and return a Graph Object. (SVG)
	 * @param {point[]} points, array of points.
	 * @param {string} containerID, id of the DOM Node container.
	 * @param {function} onClickCallback, function to call when we received a click to change the graph.
	 * @param {string} speedSpan, id of the DOM Node to print the speed.
	 */
	constructor(points, canvasID, onClickCallback, speedSpan) {
		this.id = Math.random()
			.toString(36)
			.substr(2, 9);
		this.points = points;
		this.samples = points.length;
		// Correct the null error with Interpolation.
		this.interpolate();
		// Data augmentation. With Interpolation.
		this.augmentation();
		// Index bind to the video.
		this.indexBinding = 0;
		// Function called after a click on the graph.
		this.onClickCallback = onClickCallback;
		// Get the DOM Node to print the speed.
		this.speedSpan = document.querySelector("#" + speedSpan);
		// Create the canvas.
		this.canvas = document.getElementById(canvasID);
		this.ctx = this.canvas.getContext("2d");

		// Test
		this.drawChart();
		this.bindEvent();
	}

	interpolate() {
		// Interpolation linéaire
		for (let i = 0; i < this.points.length; i++) {
			this.points[i];
			if (this.points[i].y === null) {
				//* Conjecture, P-1 != null.
				// On cherche P+n avec n plus petit possible tel que P+n != null.
				for (let j = i; j < this.points.length; j++) {
					if (this.points[j].y !== null) {
						// On a trouvé Pn+1
						// On fait une interpolation linéaire entre les deux points.
						this.points[i] = lineaire(this.points[i - 1], this.points[j], i);
						// Point i a été corrigé.
						break;
					}
				}
			}
		}
	}

	augmentation() {
		// Entre deux points on fera X autres points
		let aggr = [];
		for (let j = 0; j < this.samples - 1; j++) {
			for (let i = 0; i < augmentation; i++) {
				aggr.push(lineaire(this.points[j], this.points[j + 1], j + i / augmentation));
			}
		}
		// Une fois qu'on a recup nos nouveaux points, on les ajoutes entre chaque points.
		this.points = this.points.flatMap(p => [p, ...aggr.splice(0, augmentation)]);
		this.samples = this.points.length;
	}

	drawChart() {
		const gradientFill = this.ctx.createLinearGradient(0, 100, 0, 50);
		gradientFill.addColorStop(0, "rgba(255, 255, 255,0.4)");
		gradientFill.addColorStop(1, "rgba(255, 255, 255,1)");
		// Draw the chart and the mask.
		this.chart = new Chart(this.ctx, {
			type: "line",
			data: {
				labels: [...this.points.map(p => p.x)],
				datasets: [
					{
						label: "My First dataset",
						backgroundColor: "rgba(255, 255, 255,0.2)",
						borderColor: "#414151",
						data: [...this.points.map(p => p.y)],
						pointRadius: 0
					},
					{
						label: "Second dataset",
						backgroundColor: gradientFill,
						borderColor: "rgba(52, 73, 94,1)",
						data: [...this.points.map(p => p.y)].slice(0, this.indexBinding),
						pointRadius: 0
					}
				]
			},
			// Configuration options go here
			options: chartOptions
		});
	}

	updateMaskChartPourcentage(pourcentage) {
		let index = Math.floor(pourcentage * this.samples);
		this.updateMaskChart(index);
	}

	updateMaskChart(newIndex) {
		if (newIndex > this.indexBinding) {
			// On avance
			let ecart = newIndex - this.indexBinding;
			for (let i = 0; i < ecart; i++) {
				this.chartMoveForwards();
			}
		} else {
			// On recule
			let ecart = this.indexBinding - newIndex;
			for (let i = 0; i < ecart; i++) {
				this.chartMoveBackwards();
			}
		}
		this.chart.update();
		this.printSpeed(this.points[newIndex]);
	}

	chartMoveForwards() {
		this.chart.data.datasets[1].data.push(this.points[this.indexBinding].y);
		this.indexBinding++;
	}
	chartMoveBackwards() {
		this.chart.data.datasets[1].data.pop();
		this.indexBinding--;
	}

	bindEvent() {
		this.canvas.onclick = e => {
			let activePoints = this.chart.getElementsAtEvent(e);
			let index = 0;
			if (activePoints.length > 0) {
				//get the internal index of slice in pie chart
				index = activePoints[0]["_index"];
			} else {
				/**
				 * Ok, so we use a little trick here...
				 * If the user doesnt exactly click on the value, the event doesn't return anything.
				 * To fix that, we use the chart helper to get the X value vertically.
				 */
				let mousePoint = Chart.helpers.getRelativePosition(e, this.chart.chart);
				index = this.chart.scales["x-axis-0"].getValueForPixel(mousePoint.x);
			}
			this.onClickCallback(index / this.samples);
			this.updateMaskChart(index);
		};
	}

	printSpeed(point) {
		let speed = point.y;
		// Convert m/s into km/h
		speed = Number(speed * 3.6).toFixed(1);
		this.speedSpan.innerHTML = speed + " km/h";
	}
}

export default Graph;
