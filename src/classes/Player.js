import Plyr from "plyr";

const intervalDelay = 20;

class Player {
    constructor(videoId, onProgressCallback) {
        this.player = new Plyr("#" + videoId);
        this.isPlaying = false;
        this.bindCallbacks();
        this.onProgressCallback = onProgressCallback;
    }

    getTime() {
        return this.player.currentTime;
    }

    getDuration() {
        return this.player.duration;
    }

    getTimeInPourcentage() {
        return this.player.currentTime / this.player.duration;
    }

    setTimeInPourcentage(percent) {
        this.player.currentTime = this.player.duration * percent;
    }

    progressCallback() {
        let pourcentage = this.getTimeInPourcentage();
        this.onProgressCallback(pourcentage);
    }

    bindCallbacks() {
        this.player.on("playing", e => {
            this.setInterval();
        });
        this.player.on("pause", e => {
            this.clearInterval();
        });
    }

    setInterval() {
        this.progressEvent = setInterval(() => {
            this.progressCallback();
        }, intervalDelay);
    }

    clearInterval() {
        window.clearInterval(this.progressEvent);
    }
}

export default Player;
