/**
 * Construit un objet représentant une ligne avec MapBox à partir d'objet Point.
 * @param {Point[]} points
 * @param {string} color - optional
 * @param {int} lineWidth - optional
 */
export const getLineLayer = (points, color = "#212139", lineWidth = 6) => {
	return {
		id: "path",
		type: "line",
		source: {
			type: "geojson",
			data: {
				type: "Feature",
				properties: {},
				geometry: {
					type: "LineString",
					coordinates: points.map(p => p.toArray())
				}
			}
		},
		layout: {
			"line-join": "round",
			"line-cap": "round"
		},
		paint: {
			"line-color": color,
			"line-width": lineWidth
		}
	};
};
/**
 * Construit un objet représentant un point avec MapBox à de l'id de la source (des données) du point et de l'id d'un icon.
 * @param {string} source - Id de la source de données.
 * @param {string} iconId - Id de l'icon
 * @param {Number} iconSize - optional, taille de l'icon
 * @param {string : 'center'|'top'|'bottom'|'top-left'|'top-right'|'botom-left'|'bottom-right'} iconAnchor - optional, placement de l'icon.
 */
export const getMarkerLayer = (source, iconId, iconSize = 0.25, iconAnchor = "bottom") => {
	return {
		id: "marker",
		source: source,
		type: "symbol",
		layout: {
			"icon-image": iconId,
			"icon-size": iconSize,
			"icon-anchor": iconAnchor
		}
	};
};
