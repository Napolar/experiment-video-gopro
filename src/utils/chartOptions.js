const options = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
        xAxes: [{ display: false }],
        yAxes: [{ display: false }]
    },
    legend: {
        display: false
    },
    tooltips: {
        enabled: false
    },
    event: ["click"]
};
export default options;
