import Point from "../classes/Point";
/**
 ** Connaissant deux points p1,p2
 ** on effectue une interpolation linéaire
 ** sur un troisième point d'abscisse i.
 * @param {Point} p1
 * @param {Point} p2
 * @param {int} i
 * return Point
 */
export const lineaire = (p1, p2, i) => {
    // Calcul de a (de ax + b).
    let a = (p2.y - p1.y) / (p2.x - p1.x);
    // Calcul de b (de ax + b) conaissant a.
    let b = p2.y - p2.x * a;
    // On créer et renvoie le Point.
    return new Point(i, a * i + b);
};
